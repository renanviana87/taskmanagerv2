package Util;

import org.hibernate.cfg.Configuration;
import org.hibernate.SessionFactory; 
public class HibernateUtil {
 
    private static final SessionFactory sessionFactory;
    
    static {
        try {
            // Create the SessionFactory from standard (hibernate.cfg.xml) 
            // config file.
        	sessionFactory = new Configuration().configure()
    				.buildSessionFactory();
            System.out.println("sessionFactory Construida com sucesso, inicializando aplica��o...");
        } catch (Throwable ex) {
            // Log exce��o 
            System.out.println("Falha ao criar sess�o na classe HibernateUtils." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}