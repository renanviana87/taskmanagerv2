package br.com.controler;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;



@ManagedBean(name="controller")//pode-se nomear o bean para facilitar a chamada no html
@ViewScoped
public class testeController implements Serializable{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String teste; //Variavel SEMPRE com gets e sets ou n funciona nada

	public String getTeste() {
		return teste;
	}

	public void setTeste(String teste) {
		this.teste = teste;
	}
	
	public void mostraNaTela(String teste){//sempre q for manipular dados com um metodo, passe os dados pro metodo, na mesma ordem de envio e recebimento
		
		System.out.println(teste);

	}
	
}